"""Parse command-line arguments."""
import argparse


def parse_args(extra_args):
    """Parse command-line arguments."""
    parser = argparse.ArgumentParser(
        description='Respin a failed Beaker task.'
    )

    # register all parameters from the yaml file
    for value in extra_args.values():
        try:
            parser.add_argument(*value['args'], **value['kwargs'])
        except KeyError:
            print('Likely invalid argument_table.yaml syntax!')
            raise

    parser.add_argument(
        '--dryrun',
        '--dry-run',
        action='store_true',
        help='Optional. Submit job on behalf of USERNAME. The existing user '
             'attribute in the job XML will be overridden if set. (submitting'
             ' user must be a submission delegate for job owner)',
        default=False,
    )
    parser.add_argument(
        '--submit',
        action='store_true',
        help='Mandatory. If used, submit the job to Beaker. ',
        default=False,
    )

    parser.add_argument(
        '--jobowner',
        type=str,
        help='Optional. Submit job on behalf of USERNAME. The existing user '
             'attribute in the job XML will be overridden if set. (submitting'
             ' user must be a submission delegate for job owner)',
        default=None,
    )

    parser.add_argument(
        '-j',
        '--jobid',
        type=int,
        help='Optional, mutually exclusive with -i. A Beaker job id (like '
             '1234) that is the original Beaker job that has failed task(s) '
             'to rerun.',
        default=None,
    )
    parser.add_argument(
        '-i',
        '--input',
        type=str,
        help='Optional, mutually exclusive with -j. Use an existing XML '
             'instead of a jobid to download. This can be used to '
             'consequetively modify the XML.',
        default=None
    )
    parser.add_argument(
        '-r',
        '--recipe',
        action='append',
        help='Optional, list of integers. If set, only given recipeid will '
             'be present in the output job.',
        default=[],
    )

    parser.add_argument(
        '--host',
        type=str,
        action='append',
        help='Optional, list of strings. You can specify host for each recipe'
             'by using --host <recipe id>:hostname.',
        default=[],
    )

    parser.add_argument(
        '--samehost',
        action='append',
        type=int,
        help='Optional, list of integers. Each --samehost <recipe id> will'
             ' make that recipe id use the same host.',
        default=[],
    )

    parser.add_argument(
        '-s',
        type=str,
        action='append',
        help='Optional. Name of the task from XML to repeat.',
        default=[],
    )

    parser.add_argument(
        '--mr',
        type=int,
        help='Optional. How many times to repeat the task specified by -s. '
             'Default: 1.',
        default=1,
    )

    parser.add_argument(
        '--repeat',
        type=int,
        help='Optional. If used, then failed tasks will be repeated this many'
             ' times. Default: 1.',
        default=1,
    )

    parser.add_argument(
        '-o',
        '--output',
        type=str,
        help='Optional. Path to the output file. The output file contains '
             'Beaker XML that is ready to be submitted to the Beaker. '
             'Default: beaker_respin.xml',
        default='beaker_respin.xml',
    )

    parser.add_argument(
        '--stdout',
        action='store_true',
        help='Optional. If used, the output XML is written to stdout instead '
             'of being writen to the --output filepath.',
        default=False
    )

    parser.add_argument(
        '--noreservesys',
        action='store_true',
        help='Optional. Use this switch to disable adding a reserve-if-fails'
             ' task at the end of each recipe.',
        default=False,
    )

    args = parser.parse_args()

    if not (args.input is None) ^ (args.jobid is None):
        raise RuntimeError('Use ONE of -i / -j, not both or none.')

    # convert RH emails to logins
    if args.jobowner and '@' in args.jobowner:
        args.jobowner = args.jobowner.split('@')[0]

    if args.retention_tag and args.retention_tag not in ['120days', '60days',
                                                         'active', 'active+1',
                                                         'audit', 'scratch']:
        args.retention_tag = '60days'

    return args
