bkr_respin
==========

This tool aims to improve kernel developer/maintainer experience when
investigating kernel issues. A lot of times when a Beaker job testing
kernel fails, it is common to try to rerun the failed tasks and try
to see if it reproduces easily.
Not all the developers are familiar with Beaker. To avoid having to
dig in the details of Beaker XML, this tool is provided.
There are common scenarios and common parameters that need to be set
to rerun the job.

Currently, the tool processes all the recipes in the job.
You can use parameter *--r <int>* multiple times to keep only a specified
recipeid's. Using *-j* you download and process an XML. Using *--input*,
you can process it further.

**Installation**

Currently, Python3 is required.

* pip3 install --user beautifulsoup4 lxml anymarkup
* pip3 install --user https://gitlab.com/cki-project/cki-lib/-/archive/master/cki-lib-master.zip
* (then run bkr_respin)

**Basic usage**

# Download jobid **1234**, set job whiteboard to **My Whiteboard**,
output the XML as **job.xml**. Repeat task **ltp-lite 5 times**.
Keep only recipeid **7427652** and force the host to **myhost** hostname.

$ ./bkr_respin.py -j 1234 -w "My Whiteboard" -o job.xml -s ltp-lite --mr 5 --host mymachine -r 7427652

**Notes**

If a boot test fails, it will not respin the (test) tasks after the installation.
It is up for a debate, whether this is exactly as should be, or if it should
include the tests after the boot test, that were not run.
